# BoomingApiTestAssessment_solution
Api test assessment Project to demonstrate Create(POST), Read(GET), Update(PUT), Delete (CRUD) functions for the  API “locations” Endpoint hosted on a local Fake API JWT json server.

Prerequisites 
For this project, a Fake JSON Server was setup as per the instructions on the web location at https://github.com/techiediaries/fake-api-jwt-json-server

The fake JSON server was cloned and started on my local machine


Assessment Task	
This BDD Specflow API test in C# demonstrates the CRUD functions for the Endpoint “locations” which: Create(POST), Read(GET), Update(PUT), Delete using Visual Studio.

"locations": [
	    {
	      "id": 1,
	      "name": "Location001"
	    },
	    {
	      "id": 2,
	      "name": "Location002"
	    },
	    {
	      "id": 3,
	      "name": "Location003"
	    }
	  ],


Acceptance Criteria
-	Required Nuget Packages: Specflow for the BDD Feature file.  HTTP Requests were made using RestSharp. 
-	For the Test Runner, Nunit was used

Test
-	Test that user can login to the API with Email: “techie@email.com” and Password: “techie”
-	Test GET all Locations. Assert “OK” or “200” status.
-	Test GET a Location where “id” is “1”. Then assert that “id” is “1”, “name” is “Location001” and assert “OK” or “200” status.
-	Test POST a Location where “id” is "4" and “name” is "Location004". Then perform GET “locations/4”, assert “id” is “4”, “name” is “Locations004”, and Assert “OK” or “200” status.
-	Test PUT for "locations/4" where “id” is "4" and “name” is "Location005". Then GET operation for "locations/4", assert "id" is "4", "name" is "Location005" and Assert “OK” or “200” status.
-	Test DELETE operation for "locations/4" and Assert “OK” or “200” status.
Further Development
This project is subject to further enhancements and extensions to add additional functionalities when necessary.
In a further development, an attempt was made to add a report generation mechanism to generate test report for this project using the Extent test report framework. As at the time of writing this Readme file, the work approaching its completion stage. 

This Readme file will therefore be updated on completion of each newly added functionality.
