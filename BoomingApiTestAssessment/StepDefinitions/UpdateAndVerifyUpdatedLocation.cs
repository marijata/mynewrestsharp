﻿using BoomingApiTestAssessment.CrudFunctions;
using TechTalk.SpecFlow;
using TechTalk.SpecFlow.Assist;

namespace BoomingApiTestAssessment.StepDefinitions
{
    [Binding]
    public class CrudFunctionsSteps
    {
        private readonly Functions _functions = new Functions();
        readonly ScenarioContext _scenarioContext;

        public CrudFunctionsSteps(ScenarioContext scenarioContext)
        {
            _scenarioContext = scenarioContext;
        }

        [When(@"I update a new location with the following details")]
        public void WhenIUpdateANewLocationWithTheFollowingDetails(Table table)
        {
            dynamic data = table.CreateDynamicInstance();
           _scenarioContext["response"] = _functions.PutRequest(data.Id, data.Name); 
        }        
    }
}
