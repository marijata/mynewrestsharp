﻿using BoomingApiTestAssessment.CrudFunctions;
using TechTalk.SpecFlow;

namespace BoomingApiTestAssessment.StepDefinitions
{
    [Binding]
    public class DeleteAndVerifyDeletedLocation
    {
        private readonly Functions _functions = new Functions();
        readonly ScenarioContext _scenarioContext;
        public DeleteAndVerifyDeletedLocation(ScenarioContext scenarioContext)
        {
            _scenarioContext = scenarioContext;
        }

        [When(@"I delete a location with id ""(.*)""")]
        public void WhenIDeleteALocationWithId(int id)
        {
            _functions.DeleteRequest(id);
        }
    }
}
