﻿using BoomingApiTestAssessment.CrudFunctions;
using FluentAssertions;
using System;
using TechTalk.SpecFlow;

namespace BoomingApiTestAssessment.StepDefinitions
{
    [Binding]
    public class CommonSteps
    {
        private readonly Functions _functions = new Functions();
        readonly ScenarioContext _scenarioContext;
        public CommonSteps(ScenarioContext scenarioContext)
        {
            _scenarioContext = scenarioContext;
        }


        [Then(@"System should return a success response status code of ""(.*)""")]
        public void ThenSystemShouldReturnASuccessResponseStatusCodeOf(int statusCode)
        {
            _scenarioContext["StatusCode"].Should().Be(statusCode);
        }


        [Then(@"I request to view the newly created location with id ""(.*)""")]
        [When(@"I request to view a single location with id ""(.*)""")]
        [Then(@"I request to view the updated location with id ""(.*)""")]
        [When(@"I request to view the deleted location with id ""(.*)""")]
        public void ThenIRequestToViewTheNewlyCreatedLocationWithId(int id)
        {
            var response = _functions.GetLocation(id);
            _scenarioContext["response"] = response.Content;
            _scenarioContext["StatusCode"] = response.StatusCode;

        }
    }
}
