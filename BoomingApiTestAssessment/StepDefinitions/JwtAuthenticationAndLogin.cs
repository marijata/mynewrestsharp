﻿using BoomingApiTestAssessment.CrudFunctions;
using FluentAssertions;
using TechTalk.SpecFlow;
using TechTalk.SpecFlow.Assist;

namespace BoomingApiTestAssessment.StepDefinitions
{
    [Binding]
    public class JwtAuthenticationAndLoginSteps
    {
        private readonly Functions _functions = new Functions();
        readonly ScenarioContext _scenarioContext;

        public JwtAuthenticationAndLoginSteps(ScenarioContext scenarioContext)
        {
            _scenarioContext = scenarioContext;
        }

        [Given(@"I attempt to get Jwt authenication with the following details")]
        public void GivenIAttemptToGetJwtAuthenicationWithTheFollowingDetails(Table table)
        {
            dynamic data = table.CreateDynamicInstance();
            _scenarioContext["StatusCode"] = (int)_functions.JwtAuthentication(data.Email, data.Password).StatusCode;
        }
    }
}
