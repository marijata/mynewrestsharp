﻿using BoomingApiTestAssessment.CrudFunctions;
using System;
using TechTalk.SpecFlow;

namespace BoomingApiTestAssessment.StepDefinitions
{
    [Binding]
    public class GetSingleLocation
    {
        private readonly Functions _functions = new Functions();
        readonly ScenarioContext _scenarioContext;

        public GetSingleLocation(ScenarioContext scenarioContext)
        {
            _scenarioContext = scenarioContext;
        }
    }
}
