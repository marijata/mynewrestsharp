﻿using BoomingApiTestAssessment.CrudFunctions;
using BoomingApiTestAssessment.Model;
using FluentAssertions;
using Newtonsoft.Json;
using TechTalk.SpecFlow;
using TechTalk.SpecFlow.Assist;

namespace BoomingApiTestAssessment.StepDefinitions
{
    [Binding]
    public class AddAndVerifyNewLocationSteps
    {
        private readonly Functions _functions = new Functions();
        readonly ScenarioContext _scenarioContext;

        public AddAndVerifyNewLocationSteps(ScenarioContext scenarioContext)
        {
            _scenarioContext = scenarioContext;
        }

        [When(@"I add a new location with the following details")]
        public void WhenIAddANewLocationWithTheFollowingDetails(Table table)
        {
            dynamic data = table.CreateDynamicInstance();
            _scenarioContext["StatusCode"] = (int)_functions.AddLocation(data.Id, data.Name).StatusCode;
        }

        [Then(@"I should see the location id ""(.*)"" and location name ""(.*)""")]
        public void ThenIShouldSeeTheLocationIdAndLocationName(int id, string name)
        {
            var responseId = JsonConvert.DeserializeObject<Locations>(_scenarioContext["response"].ToString());
            responseId.id.Should().Be(id);
            responseId.name.Should().Be(name);
        }

        [When(@"I request to view all locations")]
        public void WhenIRequestToViewAllLocations()
        {
            _scenarioContext["StatusCode"] = (int)_functions.GetAllLocations().StatusCode;
        }
    }
}
