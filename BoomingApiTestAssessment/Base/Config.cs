﻿using RestSharp;
using System;

namespace BoomingApiTestAssessment.Base
{
    public abstract class Config
    {
        public static Uri BaseUrl { get; set; }
        public static IRestResponse Response { get; set; }
        public static RestRequest Request { get; set; }
        public static RestClient Client { get; set; } = new RestClient();
        public static string AccessToken { get; set; }
    }
}