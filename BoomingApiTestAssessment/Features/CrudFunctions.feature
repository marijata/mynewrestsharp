﻿Feature: CrudFunctions
	In order to test API functionalities
	As a consumer of Api functions
	I want to be able to get JwtAuthentication and perform Api CRUD functions

Background: 
     Given I attempt to get Jwt authenication with the following details
		| Email            | Password |
		| techie@email.com | techie   |  

@LoginAuthentication
Scenario: 01 Verify that Jwt authenication is successful
	 Then System should return a success response status code of "200"

@AddLocation
Scenario: 02 Add and verify newly added location
	 When I add a new location with the following details
		| Id | Name        |
		| 4  | Location004 | 
	 Then System should return a success response status code of "201"
	 And I request to view the newly created location with id "4"
	 Then System should return a success response status code of "200"
	 And  I should see the location id "4" and location name "Location004"

@GetAllLocations
Scenario: 03 Get all locations
     When I request to view all locations
	 Then System should return a success response status code of "200"

@GetSingleLocation
Scenario: 04 Get a single location
     When I request to view a single location with id "1"
	 Then System should return a success response status code of "200"
	 And  I should see the location id "1" and location name "Location001"

@UpdateLocation
Scenario: 05 Update a location
	 When I update a new location with the following details
		| Id | Name        |
		| 4  | Location005 | 
	 Then System should return a success response status code of "200"
	 And I request to view the updated location with id "4"
	 Then I should see the location id "4" and location name "Location005"

@DeleteLocation
Scenario: 06 Delete a location
	 When I delete a location with id "4"
	 And I request to view the deleted location with id "4"
	 Then System should return a success response status code of "404"

