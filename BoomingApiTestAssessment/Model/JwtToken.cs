﻿using Newtonsoft.Json;

namespace BoomingApiTestAssessment.Model
{
    public class JwtToken
    {
        [JsonProperty(PropertyName = "access_token")]
        public string AccessToken { get; set; }
    }
}
