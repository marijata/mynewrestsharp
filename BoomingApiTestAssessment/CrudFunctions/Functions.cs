﻿using BoomingApiTestAssessment.Base;
using BoomingApiTestAssessment.Model;
using BoomingApiTestAssessment.Utilities.Resources;
using RestSharp;
using RestSharp.Authenticators;

namespace BoomingApiTestAssessment.CrudFunctions
{
    public class Functions 
    {
        public IRestResponse JwtAuthentication(string email, string password)
        {
           Config.Request = new RestRequest(EndPoints.Login, Method.POST);
            Config.Request.AddHeader("Content-Type", "application/json");
            Config.Request.AddJsonBody(new { email = email, password = password });
            var response = Config.Client.Execute<JwtToken>(Config.Request);
            var jwtAuth = new JwtAuthenticator(response.Data.AccessToken);
            Config.Client.Authenticator = jwtAuth;

            return response;
        }

        public IRestResponse AddLocation(int id, string name)
        {
            Config.Request = new RestRequest(EndPoints.Add, Method.POST);
            Config.Request.RequestFormat = DataFormat.Json;
            Config.Request.AddJsonBody(new Locations { id = id, name = name });
            var response = Config.Client.Execute<Locations>(Config.Request);

            return response;
        }

        public IRestResponse GetAllLocations()
        {
            Config.Request = new RestRequest(EndPoints.GetLocations, Method.GET);
            Config.Request.RequestFormat = DataFormat.Json;
            Config.Request.AddParameter("application/json", ParameterType.RequestBody);
            var response = Config.Client.Execute<Locations>(Config.Request);

            return response;
        }

        public IRestResponse GetLocation(int id)
        {
            Config.Request = new RestRequest(EndPoints.Crud, Method.GET);
            Config.Request.RequestFormat = DataFormat.Json;
            Config.Request.AddUrlSegment("id", id);
            Config.Request.AddParameter("application/json", ParameterType.RequestBody);
           var response = Config.Client.Execute<Locations>(Config.Request);

            return response;
        }

        public IRestResponse PutRequest(int id, string locationName)
        {
            Config.Request = new RestRequest(EndPoints.Crud, Method.PUT);
            Config.Request.RequestFormat = DataFormat.Json;
            Config.Request.AddUrlSegment("id", id);
            Config.Request.AddJsonBody(new { name = locationName });
            var response = Config.Client.Execute<Locations>(Config.Request);

            return response;
        }

        public IRestResponse DeleteRequest(int id)
        {
            Config.Request = new RestRequest(EndPoints.Crud, Method.DELETE);
            Config.Request.RequestFormat = DataFormat.Json;
            Config.Request.AddUrlSegment("id", id);
            var response = Config.Client.Execute<Locations>(Config.Request);

            return response;
        }
    }
}
